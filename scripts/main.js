/* Erzeugt ein Node-Element für die html-Tabelle, die eine Preisliste enthält, die wie folgt aussieht:
   | Anzahl | Kanone Deluxe  | Flammenwerfer | Feuerlöscher |
   ----------------------------------------------------------
   | 1      | 5400           | 400           | 150          |
   | 2      | 10800          | 800           | 300          |*/
function createPriceListTable(nrOfEntries, products) {
  // Ihr Code hier
}

let elBody = document.querySelector('body');
let elScript = document.querySelector('body script');
let priceList = createPriceListTable(7, [
  { product: 'Kanone Deluxe', price: 5400 },
  { product: 'Flammenwerfer', price: 400 },
  { product: 'Feuerlöscher', price: 150 },
]);
elBody.insertBefore(priceList, elScript);
